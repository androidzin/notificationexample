package com.androidzin.notification;

import com.example.notification.R;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {

	private int NOTIFICATION_ID;
	private NotificationManager notificationManager ;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       
        Button notifyOld = (Button) findViewById(R.id.notifyButtonOld);
        Button notifyNew = (Button) findViewById(R.id.notifyButtonNew);
        Button increment = (Button) findViewById(R.id.incrementButton);
        increment.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				NOTIFICATION_ID++;	
			}
		});
        notifyOld.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				simpleNotification(random());
			}
		});
        notifyNew.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				customNotification(random());
			}
		});
     
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

    }
    
    /**
     * @return A MyParcel instance with a random value as data
     */
    private MyParcel random(){
    	MyParcel parcel = new MyParcel();
    	String s = String.valueOf(Math.random() * 1000);
    	System.out.println("value: " + s);
    	parcel.writeData(s);
    	return parcel;
    }
    
    /**
     * Notify the system in a old way. Android 2.3 and older
     * @param message
     */
    private void simpleNotification(MyParcel message) {
		String title = "Title";
		String content = "Content";
		String ticker = "Ticker";
		
		Intent  toBeNotifiedIntent = new Intent(this, ReceiverActivity.class);
		toBeNotifiedIntent.putExtra("parcel", (Parcelable) message);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, toBeNotifiedIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		Notification notification = new Notification(R.drawable.ic_launcher, ticker, System.currentTimeMillis());
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.setLatestEventInfo(this, title, content, pendingIntent);
		notificationManager.notify(NOTIFICATION_ID, notification);
		
	}
    
    /**
     * Notify the system in a new way. Android 4.0 and earlier
     * @param message
     */
    private void customNotification(MyParcel message) {
    	Intent  toBeNotifiedIntent = new Intent(this, ReceiverActivity.class);
    	toBeNotifiedIntent.putExtra("parcel", (Parcelable) message);
    	String title = "Title";
		String content = "Content";
		String ticker = "Ticker";
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, toBeNotifiedIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
		builder.setSmallIcon(R.drawable.ic_action_search);
		builder.setContentTitle(title);
		builder.setContentIntent(pendingIntent);
		builder.setContentText(ticker);
		builder.addAction(R.drawable.ic_launcher, title, pendingIntent);
			
		Notification notification = builder.build();
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		
		notificationManager.notify(NOTIFICATION_ID, notification);
    }
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
}
