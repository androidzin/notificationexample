package com.androidzin.notification;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;

public class MyParcel implements Parcelable{

	private String someData;

	@Override
	public int describeContents() {
		return 0;
	}

	public MyParcel() {
	}

	public MyParcel(Parcel in) {
		readFromParcel(in);
	}

	@Override
	public void writeToParcel(Parcel parcel, int arg1) {
		parcel.writeString(someData);
	}

	public void readFromParcel(Parcel in) {
		someData = in.readString();
	}

	public static Parcelable.Creator<MyParcel> CREATOR = new Creator<MyParcel>() {
		@Override
		public MyParcel[] newArray(int size) {
			return new MyParcel[size];
		}

		@Override
		public MyParcel createFromParcel(Parcel source) {
			return new MyParcel(source);
		}
	};

	public void writeData(String string) {
		someData = string;
	}
	
	public String getData(){
		return someData;
	}
	
}