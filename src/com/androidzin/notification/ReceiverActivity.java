package com.androidzin.notification;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

public class ReceiverActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		MyParcel c = (MyParcel) this.getIntent().getParcelableExtra("parcel");
		if (c != null) {
			Toast.makeText(this,"Parcel Data " + c.getData(),Toast.LENGTH_SHORT).show();
		}
		finish();
	}
}
